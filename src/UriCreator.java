import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class UriCreator {

	public ArrayList<String> createUris(String basicUri, Date startDate, Date endDate) {

		ArrayList<String> datesList = createDates(startDate, endDate);
		ArrayList<String> uris = new ArrayList<String>();

		for (String date : datesList) {
			uris.add(basicUri + date);
		}

		return uris;
	}

	public ArrayList<String> createDates(Date startDate, Date endDate) {

		ArrayList<String> datesList = new ArrayList<String>();

		Calendar start = Calendar.getInstance();
		start.setTime(startDate);

		Calendar end = Calendar.getInstance();
		end.setTime(endDate);

		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

		for (start.getTime(); start.before(end); start.add(Calendar.DATE, 1)) {

			Date dt = start.getTime();

			datesList.add(sdf.format(dt));
		}

		return datesList;
	}

}
