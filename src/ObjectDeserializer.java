import java.util.ArrayList;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

public class ObjectDeserializer {

	public ArrayList<ValCurs> deserializeAll(ArrayList<String> allXmls) {

		ArrayList<ValCurs> deserialized = new ArrayList<ValCurs>();

		for (String xml : allXmls) {
			deserialized.add(deserialize(xml));
		}

		return deserialized;
	}

	public ValCurs deserialize(String xml) {

		XStream xstream = new XStream(new StaxDriver());

		xstream.processAnnotations(ValCurs.class);
		xstream.processAnnotations(Valute.class);

		xstream.alias("ValCurs", ValCurs.class);
		xstream.alias("Valute", Valute.class);

		return (ValCurs) xstream.fromXML(xml);
	}

}
