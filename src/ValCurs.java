import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("ValCurs")
public class ValCurs {

	public String date;

	@XStreamImplicit
	public ArrayList<Valute> valute = new ArrayList<Valute>();

}
