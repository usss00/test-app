import java.util.ArrayList;

import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;

public class XmlEndpointReader {

	private ArrayList<String> allUris;

	public XmlEndpointReader(ArrayList<String> allUris) {
		this.allUris = allUris;
	}

	public ArrayList<String> giveAllReadXmls() {

		ArrayList<String> readedList = new ArrayList<String>();

		for (int i = 0; i < allUris.size(); i++) {
			String readXmlData = readEndpoint(allUris.get(i));

			readedList.add(readXmlData);
		}

		return readedList;
	}

	private String readEndpoint(String endpoint) {

		Content content = null;
		try {
			content = Request.Get(endpoint).execute().returnContent();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return content.asString();
	}

}