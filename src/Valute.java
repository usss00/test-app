import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Valute")
public class Valute {
	public String NumCode;
	public String CharCode;
	public String Nominal;
	public String Name;
	public String Value;
}