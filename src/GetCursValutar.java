
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.Gson;




public class GetCursValutar {

	public static void main(String[] args) throws InvalidFormatException {

		PropertiesReader.write();
		UriCreator creator = new UriCreator();

		String propread = PropertiesReader.read();

		Date startDate = new GregorianCalendar(2017, Calendar.FEBRUARY, 1).getTime();
		Date endDate = new GregorianCalendar(2017, Calendar.FEBRUARY, 11).getTime();

		ArrayList<String> ListOfDates = creator.createDates(startDate, endDate);
		String LOfDates = ListOfDates.toString();

		ArrayList<String> listOfUris = creator.createUris(propread, startDate, endDate);
		XmlEndpointReader reader = new XmlEndpointReader(listOfUris);

		ArrayList<String> listOfXmls = reader.giveAllReadXmls();
		String LOfXml = listOfXmls.toString();
		
		ObjectDeserializer deser = new ObjectDeserializer();
		
		ArrayList<ValCurs> listOfJavaObjects = deser.deserializeAll(listOfXmls);

		for (ValCurs curs : listOfJavaObjects) {
			System.out.println(curs.date);

			System.out.println("Valutes: ");

			for (Valute val : curs.valute) {
				System.out.println(val.Name);
				System.out.println(val.Nominal);
				System.out.println(val.Value);
			}
		}

		  Gson gson = new Gson();

	      //convert java object to JSON format
	      String json = gson.toJson(listOfJavaObjects);
	      System.out.println(json);
	      
	     

//	      String f1 = new String("E://TestExport.xlsx");
//
//	      XSSFWorkbook workbook = new XSSFWorkbook();
//
//	      XSSFSheet sheet = workbook.getSheetAt(0);
//	      //Create rows
//	      XSSFRow row0 = sheet.createRow(1);
//	      
//	      XSSFCell r0c0 = row0.createCell(1);
//	      r0c0.setCellValue("Date");
//	      XSSFCell r0c1 = row0.createCell(2);
//	      r0c1.setCellValue("XML");
//	      XSSFCell r0c2 = row0.createCell(3);
//	      r0c2.setCellValue("JSON");
//
//          XSSFRow row1 = sheet.createRow(2);
//	      
//	      XSSFRow row00 = sheet.getRow(2);
//	      XSSFCell r1c00 = row00.createCell(1);
//	      r1c00.setCellValue(LOfDates);
//	      XSSFCell r1c01 = row00.createCell(2);
//	      r1c00.setCellValue(LOfXml);
//	      XSSFCell r1c02 = row00.createCell(3);
//	      r1c00.setCellValue( json);
//
//	      try {
//	    	    FileOutputStream fileOut = new FileOutputStream("TestExport1.xls");
//	    	    workbook.write(fileOut);
//	    	    fileOut.close();
//	    	} catch (IOException ex) {
//	    	    Logger.getAnonymousLogger();
//	    	}
//	    	System.out.println("Your excel file has been generated!");
	      
	      String FILE_NAME = "/TestExport.csv";

	          XSSFWorkbook workbook = new XSSFWorkbook();
	          XSSFSheet sheet = workbook.createSheet("Export");
	          Object[][] datatypes = {
	                  {"Data", "Xml", "Json"},
	                  {LOfDates, LOfXml, json},
	          };

	          int rowNum = 0;
	          System.out.println("Creating excel");

	          for (Object[] datatype : datatypes) {
	              Row row = sheet.createRow(rowNum++);
	              int colNum = 0;
	              for (Object field : datatype) {
	                  Cell cell = row.createCell(colNum++);
	                  sheet.setColumnWidth(0, 1000000);
	                  if (field instanceof String) {
	                      cell.setCellValue((String) field);
	                  } else if (field instanceof Integer) {
	                      cell.setCellValue((Integer) field);
	                  }
	              }
	          }

	          try {
	              FileOutputStream outputStream = new FileOutputStream(FILE_NAME);
	              workbook.write(outputStream);
	              outputStream.close();
	          } catch (FileNotFoundException e) {
	              e.printStackTrace();
	          } catch (IOException e) {
	              e.printStackTrace();
	          }

	          System.out.println("Done");
	      }
	}

