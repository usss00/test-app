import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesReader {

	public static void write() {

		String endpoint = "https://www.bnm.md/en/official_exchange_rates?get_xml=1&date="; // 01.02.2017

		try {
			Properties props = new Properties();
			props.setProperty("endpoint", endpoint);

			File f = new File("properties.config");
			OutputStream out = new FileOutputStream(f);
			props.store(out, "endpoint uri");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String read() {

		Properties props = new Properties();
		InputStream is = null;

		try {
			File f = new File("properties.config");
			is = new FileInputStream(f);
			props.load(is);
		} catch (Exception e) {
			is = null;
		}

		return props.getProperty("endpoint", "");
	}

}